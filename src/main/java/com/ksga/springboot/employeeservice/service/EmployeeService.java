package com.ksga.springboot.employeeservice.service;

import com.ksga.springboot.employeeservice.model.employee.EmployeeDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeService {
    //TODO: get all employees
    Page<EmployeeDto> getAllEmployees(Pageable pageable);

    //TODO: save employee
    EmployeeDto saveEmployee(EmployeeDto employeeDto);

    //TODO: get employee by id
    EmployeeDto getEmployeeById(int id);

    //TODO: update employee by id
    EmployeeDto updateEmployeeById(int id, EmployeeDto employeeDto);

    //TODO: delete employee
    EmployeeDto deleteEmployeeById(int id);

    //TODO: get employee by name
    Page<EmployeeDto> getEmployeeByName(String name, Pageable pageable);
}
