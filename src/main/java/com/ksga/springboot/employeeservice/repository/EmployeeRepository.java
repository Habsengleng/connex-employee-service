package com.ksga.springboot.employeeservice.repository;

import com.ksga.springboot.employeeservice.model.employee.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(exported = false)
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    Page<Employee> findAllByStatusIsTrue(Pageable pageable);
    Employee findEmployeeByIdAndStatusIsTrue(int id);
    Page<Employee> findAllByFullNameContainingIgnoreCase(String name, Pageable pageable);
}
